IsomorphicGit_установить = function(мир, имяМодуля)
{
    var м = мир.модули.модульПоИмени(имяМодуля);

    var isogit = м.содержимое["/isomorphic-git_1.7.2/index.umd.min.js"];
    var http = м.содержимое["/isomorphic-git_1.7.2/index.js"];
    var lfs = м.содержимое["/isomorphic-git_1.7.2/lightning-fs.min.js"];

    function загрузитьСкрипт(код)
    {
        var скрипт = document.createElement("script");
        скрипт.innerHTML = код;
        document.body.appendChild(скрипт);
    }

    загрузитьСкрипт(lfs);
    загрузитьСкрипт(isogit);

    eval(http);
    мир.гитHTTP = { request };
};

IsomorphicGit_install = IsomorphicGit_установить;
