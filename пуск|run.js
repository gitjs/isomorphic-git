

УстановитьIsomorphicGit = function(мир)
{
    IsomorphicGit_установить(мир, "isomorphic-git");
};


/*
// // // //


Закомитать = function(мир)
{
    (async() => {

        console.debug("01")

        var дата = new Date();
        await мир.fs.promises.writeFile("/abc", дата.toString(), "utf8");
        await git.add({
            fs: мир.fs,
            dir: "/",
            filepath: "abc"
        });
        var sha = await git.commit({
            fs: мир.fs,
            dir: "/",
            message: мир.сообщение,
            author: мир.автор,
        })

        console.debug("commit sha:", sha);

		await git.push({
            fs: мир.fs,
            http: мир.гитHTTP,
            dir: "/",
            remote: мир.источник,
            ref: мир.ветка,
            onAuth: function() {
                return {
                    "username": мир.пользователь,
                    "password": мир.пароль,
                };
            },
        });

        console.debug("закомитали")

    })();
};


// // // //


ВывестиСписокФайлов = function(мир)
{
    console.debug("файлы", мир.файлы);
};


// // // //


/*
СоздатьВетку = function(мир)
{
    (async() => {

        console.debug("01");

		await git.checkout({
            fs: мир.fs,
            dir: мир.директория,
            ref: мир.ветка,
        });

        console.debug("02");

        console.log("создали ветку");
        мир.уведомить("создали ветку");

    })();
};


// // // //


СоздатьВеткуДистанционно = function(мир)
{
    (async() => {

        console.debug("01")

		await git.push({
            fs: мир.fs,
            http: мир.гитHTTP,
            dir: мир.директория,
            remote: мир.источник,
            ref: мир.ветка,
            onAuth: function() {
                return {
                    "username": мир.пользователь,
                    "password": мир.пароль,
                };
            },
        });

        console.debug("02")
        console.log("создали ветку дистанционно");
        мир.уведомить("создали ветку дистанционно");

    })();
};


// // // //


СоздатьВеткуЛокально = function(мир)
{
    (async() => {

		await git.branch({
            fs: мир.fs,
            dir: мир.директория,
            ref: мир.ветка,
            checkout: true,
        });

        console.log("создали ветку локально");
        мир.уведомить("создали ветку локально");

    })();
};


// // // //


ПроверитьНаличиеВетки = function(мир)
{
    (async() => {

		var ветки = await git.listBranches({
            fs: мир.fs,
            dir: мир.директория,
        });

        if (ветки.includes(мир.ветка))
        {
            мир.уведомить("ветка присутствует");
        }
        else
        {
            мир.уведомить("ветка отсутствует");
        }

    })();
};
*/


// // // //


УведомитьОЗавершенииОбновления = function(мир)
{
    console.debug("завершили обновление");
};


// // // //


ОбновитьХранилище = function(мир)
{
    (async() => {

        try
        {
            await git.pull({
                fs: мир.fs,
                http: мир.гитHTTP,
                dir: "/",
                url: мир.хранилище,
                depth: 1,
                username: мир.пользователь,
                password: мир.пароль,
                author: мир.автор,
            });

            мир.уведомить("обновили хранилище");
        }
        catch (ошибка)
        {
            мир.ошибка = ошибка;
            мир.уведомить("не обновили хранилище");
        }

    })();
};


// // // //


УведомитьОЗавершенииКлонирования = function(мир)
{
    console.debug("завершили клонирование");
};


// // // //


СклонироватьХранилище = function(мир)
{
    (async() => {

		await git.clone({
            fs: мир.fs,
            http: мир.гитHTTP,
            dir: "/",
            url: мир.хранилище,
            depth: 1,
            username: мир.пользователь,
            password: мир.пароль,
        });

        мир.уведомить("склонировали хранилище");

    })();
};


// // // //


/*
УведомитьОбОшибкеПодготовкиДиректории = function(мир)
{
    var сообщение = `ОШИБКА Не удалось подготовить директорию для хранилища Гит | ERROR Could not prepare a directory for Git repository ${мир.ошибка}`;
    document.body.innerHTML += `<p>${сообщение}</p>`;
    console.error(сообщение);
};


// // // //


ПодготовитьФайловуюСистему = function(мир)
{
    // Очищаем IndexedDB.
    мир.fs = new LightningFS(мир.имяФС, { wipe: true });
};


// // // //


ПроверитьНаличиеКопии = function(мир)
{
    (async() => {

        мир.файлы = await мир.fs.promises.readdir("/");
        if (мир.файлы.length)
        {
            мир.уведомить("копия присутствует");
        }
        else
        {
            мир.уведомить("копия отсутствует");
        }

    })();
};


// // // //


ЗадатьНастройки = function(мир)
{
    мир.имяФС = "fs";
    мир.пользователь = "kornerr";
    мир.пароль = localStorage.getItem("пароль");
    мир.автор = {
        "name": "Михаил Капелько",
        "email": "kornerr@gmail.com",
    };
    мир.хранилище = "https://git.opengamestudio.org/kornerr/isomorphic-git-module-test";
    //мир.хранилище = "https://git.opengamestudio.org/kornerr/gitjs-privet-mir";
    мир.ветка = "master";
    мир.источник = "origin";
    мир.сообщение = "Изменения от isomorphic-git";
};


*/
